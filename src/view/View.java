package view;

import controller.Keyboard;
import game.Audio;
import model.Model;
import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Matteo on 14/03/2017.
 */
public class View extends JPanel {

    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final int MARIO_FREQUENCY = 3;
    private static final int MUSHROOM_FREQUENCY = 3;
    private static final int TURTLE_FREQUENCY = 3;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;

    private enum GameState {INIT, GAME_LOOP};
    private GameState currentState;

    private Image imgBackground1, imgBackground2, castle, start, imgFlag, imgCastle;

    private int background1PosX, background2PosX;

    private Model gameModel;

    public View(Model model){
        currentState = GameState.INIT;
        loadImage();

        gameModel = model;
        background1PosX = -50;
        background2PosX = 750;

        //this.addKeyListener(new Keyboard(this));
        this.setVisible(true);

    }

    private void loadImage(){
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);
    }

    @Override
    public void paintComponent(Graphics g2) {
        super.paintComponent(g2);

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - gameModel.getxPosScreen(), 95, null);
        g2.drawImage(this.start, 220 - gameModel.getxPosScreen(), 234, null);
        g2.drawImage(this.imgFlag, FLAG_X_POS - gameModel.getxPosScreen(), FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - gameModel.getxPosScreen(), CASTLE_Y_POS, null);

        for (int i = 0; i < gameModel.getObstacles().size(); i++) {
            g2.drawImage(gameModel.getObstacles().get(i).getImgObj(), gameModel.getObstacles().get(i).getX(),
                    gameModel.getObstacles().get(i).getY(), null);
        }

        for (int i = 0; i < gameModel.getPieces().size(); i++) {
            g2.drawImage(gameModel.getPieces().get(i).getImgObj(), gameModel.getPieces().get(i).getX(),
                    gameModel.getPieces().get(i).getY(), null);
        }

        if (this.gameModel.getMario().isJumping()) {
            g2.drawImage(this.gameModel.getMario().doJump(), this.gameModel.getMario().getX(),
                    this.gameModel.getMario().getY(), null);
        } else {
            g2.drawImage(this.gameModel.getMario().walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY),
                    this.gameModel.getMario().getX(), this.gameModel.getMario().getY(), null);
        }
        if (this.gameModel.getMushroom().isAlive()) {
            g2.drawImage(this.gameModel.getMushroom().walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY),
                    this.gameModel.getMushroom().getX(), this.gameModel.getMushroom().getY(), null);
        } else {
            g2.drawImage(this.gameModel.getMushroom().deadImage(), this.gameModel.getMushroom().getX(),
                    this.gameModel.getMushroom().getY() + MUSHROOM_DEAD_OFFSET_Y, null);
        }
        if (this.gameModel.getTurtle().isAlive()) {
            g2.drawImage(this.gameModel.getTurtle().walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY),
                    this.gameModel.getTurtle().getX(), this.gameModel.getTurtle().getY(), null);
        } else {
            g2.drawImage(this.gameModel.getTurtle().deadImage(), this.gameModel.getTurtle().getX(),
                    this.gameModel.getTurtle().getY() + TURTLE_DEAD_OFFSET_Y, null);
        }

        this.updateBackgroundOnMovement();
    }

    public void updateBackgroundOnMovement() {
        gameModel.setxPosScreen();
        if (gameModel.getxPosScreen() >= 0 && gameModel.getxPosScreen() <= 4600) {

            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - gameModel.getMovement();
            this.background2PosX = this.background2PosX - gameModel.getMovement();
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void playMoneySound(){
        Audio.playSound(Res.AUDIO_MONEY);
    }

    public void setBackground1PosX(int value){
        this.background1PosX = value;
    }

    public void setBackground2PosX(int value){
        this.background2PosX = value;
    }

    public Model getGameModel(){
        return this.gameModel;
    }
}
