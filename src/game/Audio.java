package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    public Audio(String path) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(path));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public void play() {
        clip.start();
    }

    public static void playSound(String path) {
        Audio audio = new Audio(path);
        audio.play();
    }
}
