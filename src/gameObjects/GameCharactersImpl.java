package gameObjects;

import model.Model;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Matteo on 17/03/2017.
 */
public class GameCharactersImpl extends GameComponent implements GameCharacters {

    private static final int PROXIMITY_MARGIN = 10;

    private boolean characterAlive, characterToRight, characterMoving, toRight;

    private int counter, heightLimit = 0, floorOffsetY = 293;

    public GameCharactersImpl(Point position, Dimension dimension, String path, Model model) {
        super(position, dimension, path, model);
        this.characterAlive = true;
        this.characterToRight = true;
        this.counter = 0;
        this.characterMoving = false;
        this.toRight = true;
    }

    @Override
    public boolean isAlive() {
        return this.characterAlive;
    }

    @Override
    public boolean isToRight() {
        return this.characterToRight;
    }

    @Override
    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.characterMoving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    @Override
    public boolean hitAhead(GameComponent og) {
        if (this.getX() + this.getWidth() < og.getX() || this.getX() + this.getWidth() > og.getX() + 5 ||
                this.getY() + this.getHeight() <= og.getY() || this.getY() >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    @Override
    public boolean hitBack(GameComponent og) {
        if (this.getX() > og.getX() + og.getWidth() || this.getX() + this.getWidth() < og.getX() + og.getWidth() - 5 ||
                this.getY() + this.getHeight() <= og.getY() || this.getY() >= og.getY() + og.getHeight()) {
            return false;
        } else
            return true;
    }

    @Override
    public boolean hitBelow(GameComponent og) {
        if (this.getX() + this.getWidth() < og.getX() + 5 || this.getX() > og.getX() + og.getWidth() - 5 ||
                this.getY() + this.getHeight() < og.getY() || this.getY() + this.getHeight() > og.getY() + 5) {
            return false;
        } else
            return true;
    }

    @Override
    public boolean hitAbove(GameComponent og) {
        if (this.getX() + this.getWidth() < og.getX() + 5 || this.getX() > og.getX() + og.getWidth() - 5 ||
                this.getY() < og.getY() + og.getHeight() || this.getY() > og.getY() + og.getHeight() + 5) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isNearby(GameObjects obj) {
        return false;
    }

    @Override
    public boolean isNearby(GameComponent obj) {
        if ((this.getX() > obj.getX() - PROXIMITY_MARGIN &&
                this.getX() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > obj.getX() - PROXIMITY_MARGIN
                        && this.getX() + this.getWidth() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }

    @Override
    public int getFloorOffsetY() {
        return this.floorOffsetY;
    }

    @Override
    public int getHeightLimit() {
        return this.heightLimit;
    }

    @Override
    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    @Override
    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    @Override
    public void setAlive(boolean alive) {
        this.characterAlive = alive;
    }

    @Override
    public void setMoving(boolean moving) {
        this.characterMoving = moving;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.characterToRight = toRight;
    }
}
