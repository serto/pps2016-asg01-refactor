package gameObjects;

import model.Model;
import utils.Utils;

import java.awt.*;

/**
 * Created by Matteo on 18/03/2017.
 */
public class AnimatedEnemy extends GameCharactersImpl implements Runnable{

    private boolean alive = true;
    private int offsetX = 1;
    private final static int PAUSE = 3;

    private String deadImage;

    public AnimatedEnemy(Point position, Dimension dimension, String path, String dead, Model model) {
        super(position, dimension, path, model);

        this.deadImage = dead;

        Thread enemy = new Thread(this);
        enemy.start();
    }

    @Override
    public void run() {
        while (this.alive) {
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

    public void contact(GameComponent obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        super.setX(super.getX() + this.offsetX);
    }

    public Image deadImage() {
        return Utils.getImage(this.deadImage);
    }

    @Override
    public boolean isNearby(GameObjects obj) {
        return false;
    }
}
