package gameObjects;

import model.Model;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Matteo on 18/03/2017.
 */
public class MainCharacter extends GameCharactersImpl {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private final static int X_POSITION_MARIO = 300;
    private final static int Y_POSITION_MARIO = 245;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private boolean jumping;

    private int jumpingExtent;

    public MainCharacter(Model model) {
        super(new Point(X_POSITION_MARIO, Y_POSITION_MARIO),new Dimension(WIDTH, HEIGHT), Res.IMG_MARIO_DEFAULT, model);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping(){ return this.jumping; }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > super.getHeightLimit())
                this.setY(this.getY() - 4);
            else this.jumpingExtent = JUMPING_LIMIT;
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < super.getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }
        return Utils.getImage(str);
    }

    public void contact(GameComponent obj) {
        if (this.hitAhead(obj) && this.isToRight() || this.hitBack(obj) && !this.isToRight()) {
            super.getGameModel().setMovement(0);
            this.setMoving(false);
        }

        if (this.hitBelow(obj) && this.jumping) {
            super.setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(obj)) {
            super.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(obj)) {
                super.setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(obj) && !this.jumping) {
                super.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(GameComponent piece) {
        if (this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece) || this.hitBelow(piece))
            return true;

        return false;
    }
}
