package gameObjects;

import model.Model;

import java.awt.*;

/**
 * Created by Matteo on 17/03/2017.
 */
public interface GameObjects {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);

    Model getGameModel();

    Image getImgObj();

    void setImgObj(String path);

    void move();
}
