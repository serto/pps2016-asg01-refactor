package gameObjects;

import model.Model;
import utils.Res;

import java.awt.*;

public class Tunnel extends GameComponent {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(int x, int y, Model model) {
        super(new Point(x, y), new Dimension(WIDTH, HEIGHT), Res.IMG_TUNNEL, model);
    }

    @Override
    public boolean isNearby(GameComponent obj) {
        if ((this.getX() > obj.getX() - PROXIMITY_MARGIN &&
                this.getX() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > obj.getX() - PROXIMITY_MARGIN
                        && this.getX() + this.getWidth() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
