package gameObjects;

import model.Model;
import utils.Utils;

import java.awt.*;

/**
 * Created by Matteo on 17/03/2017.
 */
public abstract class GameComponent implements GameObjects {

    private Point componentPosition;
    private Dimension componentDimension;
    private Image componentImage;

    private Model gameModel;

    public GameComponent(Point position, Dimension dimension, String path, Model model){
        this.componentDimension = dimension;
        this.componentPosition = position;
        this.gameModel = model;
        this.setImgObj(path);
    }

    @Override
    public int getWidth() {
        Dimension dimension = this.componentDimension;
        return (int) dimension.getWidth();
    }

    @Override
    public int getHeight() {
        Dimension dimension = this.componentDimension;
        return (int) dimension.getHeight();
    }

    @Override
    public int getX() {
        Point position = this.componentPosition;
        return (int) position.getX();
    }

    @Override
    public int getY() {
        Point position = this.componentPosition;
        return (int) position.getY();
    }

    @Override
    public void setX(int x){
        this.componentPosition.setLocation(x, this.componentPosition.getY());
    }

    @Override
    public void setY(int y) {
        this.componentPosition.setLocation(this.componentPosition.getX(), y);
    }
    @Override
    public Image getImgObj() {
        return this.componentImage;
    }

    @Override
    public Model getGameModel(){
        return this.gameModel;
    }
    @Override
    public void setImgObj(String path) {
        this.componentImage = Utils.getImage(path);
    }

    public void move(){
        if (this.gameModel.getxPosScreen() >= 0) {
            this.componentPosition.setLocation(componentPosition.getX() - this.gameModel.getMovement(),
                    componentPosition.getY());
        }
    }

    public abstract boolean isNearby(GameComponent obj);
}
