package gameObjects;

import model.Model;

import java.awt.*;

/**
 * Created by Matteo on 17/03/2017.
 */
public interface GameCharacters extends GameObjects {
    boolean isAlive();

    boolean isToRight();

    Image walk(String name, int frequency);

    boolean hitAhead(GameComponent og);

    boolean hitBack(GameComponent og);

    boolean hitBelow(GameComponent og);

    boolean hitAbove(GameComponent og);

    boolean isNearby(GameObjects obj);

    int getFloorOffsetY();

    int getHeightLimit();

    void setFloorOffsetY(int floorOffsetY);

    void setHeightLimit(int heightLimit);

    void setAlive(boolean alive);

    void setX(int x);

    void setY(int y);

    Model getGameModel();

    void setMoving(boolean moving);

    void setToRight(boolean toRight);
}
