package gameObjects;

import model.Model;
import utils.Res;

import java.awt.*;

public class Piece extends GameComponent {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Piece(int x, int y, Model model) {
        super(new Point(x, y), new Dimension(WIDTH, HEIGHT), Res.IMG_PIECE1, model);
    }

    @Override
    public boolean isNearby(GameComponent obj) {
        if ((this.getX() > obj.getX() - PROXIMITY_MARGIN &&
                this.getX() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > obj.getX() - PROXIMITY_MARGIN
                        && this.getX() + this.getWidth() < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN))
            return true;
        return false;
    }
}
