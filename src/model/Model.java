package model;

import gameObjects.*;
import controller.Keyboard;
import view.View;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Matteo on 13/03/2017.
 */
public class Model implements Runnable {

    private final static int TIME_SLEEP = 3;
    private final static int LENGHT_MAP = 4600;

    private CreationGameObjects creationGameObjects;

    private int xPosScreen, movement;

    private View view;
    private JFrame gameFrame;

    private MainCharacter mario;
    private Goomba mushroom;
    private Koopa turtle;

    private List<GameComponent> obstacles = new ArrayList<>();
    private List<Piece> pieces = new ArrayList<>();

    public Model(JFrame gameFrame){
        mario = new MainCharacter(this);
        mushroom = new Goomba(this);
        turtle = new Koopa(this);

        this.creationGameObjects = new CreationGameObjects(this);

        this.gameFrame = gameFrame;
        view = new View(this);
        this.gameFrame.getContentPane().add(view);
        this.gameFrame.setVisible(true);

        this.gameFrame.addKeyListener(new Keyboard(this.view));

        xPosScreen = -1;
        movement = 0;

        obstacles.addAll(creationGameObjects.getObstacleTunnel());
        obstacles.addAll(creationGameObjects.getObstacleBlock());
        pieces.addAll(creationGameObjects.getObstaclePiece());
    }

    @Override
    public void run() {
        while (true) {
            //Graphics gameGraphics = gameFrame.getGraphics();
            //view.paintComponent(gameFrame.getGraphics());
            view.repaint();
            checkCollision();
            moveOjects();

            try {
                Thread.sleep(TIME_SLEEP);
            } catch (InterruptedException e) {
            }
        }
    }

    private void checkCollision(){
        for (int i = 0; i < obstacles.size(); i++) {
            if (this.mario.isNearby(this.obstacles.get(i)))

                this.mario.contact((GameComponent) this.obstacles.get(i));
            
            if (this.mushroom.isNearby(this.obstacles.get(i)))
                this.mushroom.contact((GameComponent) this.obstacles.get(i));

            if (this.turtle.isNearby(this.obstacles.get(i)))
                this.turtle.contact((GameComponent) this.obstacles.get(i));
        }

        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                this.view.playMoneySound();
                this.pieces.remove(i);
            }
        }
    }

    private void moveOjects(){
        if (this.xPosScreen >= 0 && this.xPosScreen <= LENGHT_MAP) {
            for (int i = 0; i < obstacles.size(); i++) {
                obstacles.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }
        }
    }

    public int getxPosScreen(){
        return xPosScreen;
    }

    public void setMovement(int mov){
        movement = mov;
    }

    public int getMovement(){
        return movement;
    }

    public void setxPosScreen(){
        xPosScreen = xPosScreen + movement;
    }

    public void setxPosScreen(int value) {
        xPosScreen = value;
    }

    public List<GameComponent> getObstacles(){
        return Collections.unmodifiableList(obstacles);
    }

    public List<Piece> getPieces(){
        return Collections.unmodifiableList(pieces);
    }

    public MainCharacter getMario(){
        return mario;
    }

    public Goomba getMushroom(){
        return mushroom;
    }

    public Koopa getTurtle(){
        return turtle;
    }

    public Graphics getGraphics(){
        return gameFrame.getGraphics();
    }
}
