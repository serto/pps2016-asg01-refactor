package model;

import gameObjects.Block;
import gameObjects.Piece;
import gameObjects.Tunnel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Matteo on 13/03/2017.
 */
public class CreationGameObjects {

    private List<Tunnel> obstacleTunnel = new ArrayList<>();
    private List<Block> obstacleBlock = new ArrayList<>();
    private List<Piece> obstaclePiece = new ArrayList<>();

    private Model gameModel;

    public CreationGameObjects(Model model){
        this.gameModel = model;
        addObstacleTunnel();
        addObstacleBlock();
        addObstaclePiece();
    }

    private void addObstacleTunnel(){
        obstacleTunnel.add(new Tunnel(600, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(1000, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(1600, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(1900, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(2500, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(3000, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(3800, 230, this.gameModel));
        obstacleTunnel.add(new Tunnel(4500, 230, this.gameModel));
    }

    private void addObstacleBlock(){
        obstacleBlock.add(new Block(400, 180, this.gameModel));
        obstacleBlock.add(new Block(1200, 180, this.gameModel));
        obstacleBlock.add(new Block(1270, 170, this.gameModel));
        obstacleBlock.add(new Block(1340, 160, this.gameModel));
        obstacleBlock.add(new Block(2000, 180, this.gameModel));
        obstacleBlock.add(new Block(2600, 160, this.gameModel));
        obstacleBlock.add(new Block(2650, 180, this.gameModel));
        obstacleBlock.add(new Block(3500, 160, this.gameModel));
        obstacleBlock.add(new Block(3550, 140, this.gameModel));
        obstacleBlock.add(new Block(4000, 170, this.gameModel));
        obstacleBlock.add(new Block(4200, 200, this.gameModel));
        obstacleBlock.add(new Block(4300, 210, this.gameModel));
    }

    private void addObstaclePiece(){
        obstaclePiece.add(new Piece(402, 145, this.gameModel));
        obstaclePiece.add(new Piece(1202, 140, this.gameModel));
        obstaclePiece.add(new Piece(1272, 95, this.gameModel));
        obstaclePiece.add(new Piece(1342, 40, this.gameModel));
        obstaclePiece.add(new Piece(1650, 145, this.gameModel));
        obstaclePiece.add(new Piece(2650, 145, this.gameModel));
        obstaclePiece.add(new Piece(3000, 135, this.gameModel));
        obstaclePiece.add(new Piece(3400, 125, this.gameModel));
        obstaclePiece.add(new Piece(4200, 145, this.gameModel));
        obstaclePiece.add(new Piece(4600, 40, this.gameModel));
    }

    public List<Tunnel> getObstacleTunnel(){
        return Collections.unmodifiableList(obstacleTunnel);
    }

    public List<Block> getObstacleBlock(){
        return Collections.unmodifiableList(obstacleBlock);
    }

    public List<Piece> getObstaclePiece(){
        return Collections.unmodifiableList(obstaclePiece);
    }

}
